class RegexMatcher():
    def __init__(self, text):
        self.text = text

    def find_single_match(self, pattern, group=0):
        m = pattern.search(self.text)
        if m:
            return m.groups()[group]
        else:
            return "-"

    def find_all_matches(self, pattern):
        m = pattern.findall(self.text)
        if m:
            return ', '.join(m)
        else:
            return "-"
