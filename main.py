import codecs
import os
import re
import sys

import patterns
from RegexMatcher import RegexMatcher


def processFile(filepath):
    fp = codecs.open(filepath, 'rU', 'iso-8859-2')

    content = fp.read()
    # PATTERNS
    regex_matcher = RegexMatcher(content)

    sector = regex_matcher.find_single_match(patterns.sector_pattern)

    author = regex_matcher.find_single_match(patterns.author_pattern)

    key_words = regex_matcher.find_all_matches(patterns.key_word_pattern)

    data_section = re.search(patterns.data_section_pattern, content).groups()[0]

    sentences_count = len(re.findall(patterns.sentences_pattern, data_section))

    # Chcemy miec rozne wartosci wiec wrzucamy do set
    abbreviation_count = len(set(re.findall(patterns.abbreviation_pattern, data_section)))
    int_count = len(set(re.findall(patterns.int_pattern, data_section)))
    mail_count = len(set(re.findall(patterns.mail_pattern, data_section)))

    matches = [match.group('fl') for match in re.finditer(patterns.float_pattern, data_section)]
    float_count = len(set(float(elem) for elem in matches))
    # ekstraktowanie dat formatu pierwszego (dd/mm/yyyy)z tekstu
    dates_1 = [date[0] for date in re.findall(patterns.date_pattern_1, data_section)]
    # konwertowanie ich do stringa typu ddmmyyyy
    dates_1 = [patterns.convert_to_one(date, patterns.date_format_1_pattern) for date in dates_1]

    # ekstraktowanie formatu drugiego
    dates_2 = [date[0] for date in re.findall(patterns.date_pattern_2, data_section)]
    # konwertowanie
    dates_2 = [patterns.convert_to_one(date, patterns.date_format_2_pattern) for date in dates_2]

    dates = set(dates_1 + dates_2)
    dates_count = len(dates)

    fp.close()

    print "nazwa pliku:", filepath
    print "autor:", author
    print "dzial:", sector
    print "slowa kluczowe:", key_words
    print "liczba zdan:", sentences_count
    print "liczba skrotow:", abbreviation_count
    print "liczba liczb calkowitych z zakresu int:", int_count
    print "liczba liczb zmiennoprzecinkowych:", float_count
    print "liczba dat:", dates_count
    print "liczba adresow email:", mail_count
    print "\n"


try:
    path = sys.argv[1]
except IndexError:
    print "Brak podanej nazwy katalogu"
    sys.exit(0)

tree = os.walk(path)

for root, dirs, files in tree:
    for f in files:
        if f.endswith(".html"):
            file_path = os.path.join(root, f)
            processFile(file_path)
