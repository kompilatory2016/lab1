import re


def generate_date_pattern():
    def pack_with_parentheses(s):
        return "(" + s + ")"

    separators = '[\./-]'
    alternative = "|"
    # miesiace gdzie liczba dni == 31
    days_1 = '(([012][0-9])|(30)|(31))'
    months_1 = '((0[13578])|(10)|(12))'
    # miesiace gdzie liczba dni == 30
    days_2 = '(([012][0-9])|(30))'
    months_2 = '((0[469])|(11))'
    # luty
    days_3 = '([012][0-9])'
    months_3 = '(02)'

    year = '\d{4}'

    days_months_1 = pack_with_parentheses(days_1 + separators + months_1)
    days_months_2 = pack_with_parentheses(days_2 + separators + months_2)
    days_months_3 = pack_with_parentheses(days_3 + separators + months_3)
    days_months_general_1 = pack_with_parentheses(
        days_months_1 + alternative + days_months_2 + alternative + days_months_3)

    days_months_year = pack_with_parentheses(days_months_general_1 + separators + year)

    days_months_4 = pack_with_parentheses(months_1 + separators + days_1)
    days_months_5 = pack_with_parentheses(months_2 + separators + days_2)
    days_months_6 = pack_with_parentheses(months_3 + separators + days_3)
    days_months_general_2 = pack_with_parentheses(
        days_months_4 + alternative + days_months_5 + alternative + days_months_6)

    days_months_year_2 = pack_with_parentheses(year + separators + days_months_general_2)

    # print days_months_1
    # print days_months_2
    # print days_months_3
    #
    # print '-' * 60
    # print days_months_4
    # print days_months_5
    # print days_months_6
    # print '-' * 60
    #
    # print days_months_year
    # print days_months_year_2

    # date_pattern = pack_with_parentheses(days_months_year + alternative + days_months_year_2)
    # print date_pattern

    return days_months_year, days_months_year_2


def convert_to_one(text, pattern):
    date = re.search(pattern, text)
    return date.group('day') + date.group('month') + date.group('year')


# DZIAL
author_pattern = re.compile('^<META NAME="AUTOR" CONTENT="(.+)">$', re.MULTILINE)
sector_pattern = re.compile(r'<META NAME="DZIAL" CONTENT="(.+)">', re.MULTILINE)
key_word_pattern = re.compile(r'<META NAME="KLUCZOWE_\d+" CONTENT="(.*?)">')
# Tekst pomiedzy P lub B i pierwsza META
data_section_pattern = re.compile(r'(<[PB]>.*?)<META', re.DOTALL)
sentences_pattern = re.compile(r'[A-Z0-9\'"][^<>]*?([\w]{4,}\.\s|[\n<])', re.DOTALL | re.UNICODE)
abbreviation_pattern = re.compile(r'\s[A-Za-z]{1,3}\.', re.DOTALL | re.UNICODE)
mail_pattern = re.compile(r'\w([\w]|(?<!\.)\.?)+@[\w]+((?<!\.)\.[\w]+)+')
int_pattern = re.compile(
    r'(?<!\d|\.)(-32768|-?3276[0-7]|-?327[0-5][0-9]|-?3[0-1][0-9]{2}|-?[0-2][0-9]{3}|-?[0-9]{1,4})(?!\d|\.\d)')
# old float pattern [^a-zA-Z\.]-?((\d+\.\d*)|(\d*\.\d+))([eE]\d+\s)?([^a-zA-Z\.\d]|\.\s)
float_pattern = re.compile(r'([^\.\:\d]|^)(?P<fl>-?((\d+\.\d*)|(\d*\.\d+))([Ee]\d+)?)[^\d\.]', re.MULTILINE)

date_pattern_1, date_pattern_2 = generate_date_pattern()
# regexy sluzace do konwertowania roznych formatow dat do jednego
date_format_1_pattern = re.compile(r'(?P<day>\d{2})(?P<sep>[/\.-])(?P<month>\d{2})(?P=sep)(?P<year>\d{4})')
date_format_2_pattern = re.compile(r'(?P<year>\d{4})(?P<sep>[/\.-])(?P<day>\d{2})(?P=sep)(?P<month>\d{2})')
